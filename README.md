### CS6462 Repository
-----
![](https://i.ibb.co/wNcM1RL/BDS-logo.png)

### Welcome to the CS6462 repository!
------------

Getting GIT
-------

A good link to start: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git


Managing your repository
--------
### Cloning the repo

To accomplish this, you need to clone the repository. You need to write down in your console:

`$ git clone https://gitlab.com/cs6462-ul/cs6462-ul-MSc-AI-2021.git`

--------

### Creating your branch

You need to write down in your console:

`$ git checkout -b <<NAME>>_<<GROUP_NUMBER>>_CS6462`

Replacing each attribute with your own data. This will not only create your branch, but to change from master (the main branch) into your own. You can check you're in your branch by typing:

`$ git branch`

This will show your branch name in the screen, in case everything worked out well.

-------

### Adding your work

When you finish your Jupyter notebook, you need to push your work in the gitlab repository. To do this, you need to write down in your console:

`$ git add  NAME_FILE.ipynb`

This will allow GIT to track your work. You might type:

`$ git status`

If you see your file in a green color, then you are good to go!

------------

### Commiting and pushing

One last step and we are almost there! Now you need to commit (add into the your branch the work) and push (locate your branch remotely). To do this, you need to write down:

`$ git commit -m "Jupyter notebook file from <<NAME>>"`  (where NAME is your name)

And the last command:

`$ git push origin HEAD`

### Changing branch and see others people work

To be able to see others people work, we need to change our branch. 

![](https://i.ibb.co/8bcc343/Screenshot-2021-02-04-at-11-31-55.png)

As you may see above, you have a list of the branches to pick. In the console type:

`$ git checkout <<NAME_OF_BRANCH>>` 

This should be all, if you need more help, remember to post it in the forum.

-----

Remember to post your doubts in the forum or reaching your moderators.

Greetings
